export type TaskName = string  | undefined;

export type Task = {
    name: string | undefined;
    isDone: boolean
}

export type Tasks = Task[];

