import * as React from 'react';

import {useContext} from "react";
import {ContextApp} from "./App";

import { Button } from '@mui/material';
import TextField from '@mui/material/TextField';

import {TaskName} from "../types/taskType";
import {ActionType} from "../types/stateType";

const NewTask: React.FC = () => {
    const {state, changeState} = useContext(ContextApp);

    const addTask = (event: React.FormEvent<HTMLFormElement>, task: TaskName) => {
        event.preventDefault();
        changeState?.({type: ActionType.Add, payload: task})
        changeState?.({type: ActionType.Change, payload: ''})
    }

    const changeTask = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        changeState?.({type: ActionType.Change, payload: event.target.value})
    }

    return (
        <>
            <form style={{display: 'flex', justifyContent: "space-between" , width: '100%', maxWidth: 360}} onSubmit={(event)=>addTask(event, state?.newTask)}>
                <TextField size="small" onChange={(event)=>changeTask(event)} value={state?.newTask} variant="outlined" />
                <Button size="small" variant="contained" color="success" type="submit">Add a task</Button>
            </form>
        </>
    )
};

export default NewTask;
