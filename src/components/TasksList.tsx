import * as React from 'react';

import {Task} from "../types/taskType";
import {ActionType} from "../types/stateType";
import {useContext} from "react";
import {ContextApp} from "./App";

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';

const TasksList: React.FC = () => {

    const {state, changeState} = useContext(ContextApp);

    const removeTask = (taskForRemoving: Task) => {
        changeState?.({type: ActionType.Remove, payload: taskForRemoving})
    }
    const toggleReadiness = (taskForChange: Task) => {
        changeState?.({type: ActionType.Toggle, payload: taskForChange})
    }

    return (
        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            {state?.tasks.map((task, i) => {
                const labelId = `checkbox-list-label-${task}`;

                return (
                    <ListItem
                        style={task.isDone ? {textDecoration: 'line-through'} : {}}
                        key={i}
                        secondaryAction={
                            <IconButton edge="end" onClick={()=>removeTask(task)}>
                                <DeleteForeverOutlinedIcon />
                            </IconButton>
                        }
                        disablePadding
                    >
                        <ListItemButton role={undefined} onChange={()=>toggleReadiness(task)} dense>
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    checked={task.isDone}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{ 'aria-labelledby': labelId }}
                                />
                            </ListItemIcon>
                            <ListItemText id={labelId} primary={task.name} />
                        </ListItemButton>
                    </ListItem>
                );
            })}
        </List>
    );
};

export default TasksList;
