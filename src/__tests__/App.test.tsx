import React from 'react';
import {act, fireEvent, render, waitFor, screen, cleanup} from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';


import App from '../components/App';

describe('<App />', () => {

    afterEach(cleanup)

    it('renders title Tasks', () => {
        render(<App />)
        const titleElement = screen.getByText(/Tasks/i)
        expect(titleElement).toBeInTheDocument()
    })

    it('should render right input value',  async () => {
        const { container } = render(<App/>)
        // @ts-ignore
        expect(container.querySelector('input').getAttribute('value')).toEqual('')
        act(()=>{
            // @ts-ignore
            fireEvent.change(container.querySelector('input'), {
                target: {
                    value: 'test'
                },
            })
        })
        await waitFor(() => {
            // @ts-ignore
            expect(container.querySelector('input').getAttribute('value')).toEqual('test');
        })
        act(()=>{
            // @ts-ignore
            fireEvent.click(container.querySelector('button'))
        })
        await waitFor(() => {
            // @ts-ignore
            expect(container.querySelector('input').getAttribute('value')).toEqual('');
        })
    })

})



// import * as React from 'react';
// import {shallow} from 'jest';
//
// import App from "../components/App";
// import {fireEvent, render, waitFor, cleanup} from "@testing-library/react";
//
//
// describe('<App />', () => {
//
//     afterEach(cleanup);
//
//     it('renders the component without changes', () => {
//         const component = shallow(<App />);
//         expect(component).toMatchSnapshot();
//     });
//
//     it("renders the heading", () => {
//         const result = shallow(<App />).contains(<h1>React + TypeScript</h1>);
//         expect(result).toBeTruthy();
//     });
//
// })
//
//
